﻿// HW17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

class MyClass
{
private:
	string myClassName;

public:
	MyClass() : myClassName("MyClass")
	{}

	string GetName()
	{
		return myClassName;
	}
};

class Vector
{
private:
	double x, y, z;

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void GetVector(double &_x, double &_y, double &_z)
	{
		_x = x;
		_y = y;
		_z = z;
	}

	void SetVector(double _x, double _y, double _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}

	void ShowVector()
	{
		std::cout << '(' << x << ", " << y << ", " << z << ')' << '\n';
	}

	double GetMod()
	{
		return sqrt(x * x + y * y + z * z);
	}


};

int main()
{
	// part1
	MyClass testMyClass;
	string temp = testMyClass.GetName();

    cout << testMyClass.GetName() << '\n' <<'\n';

	//part2
	Vector v1;
	cout << "V1 : \t";
	v1.ShowVector();
	cout << "V1 mod = " << v1.GetMod() << '\n';
	v1.SetVector(1, 2, 3);
	cout << "V1 : \t";
	v1.ShowVector();

	double x, y, z;
	v1.GetVector(x, y, z);
	std::cout << '(' << x << ", " << y << ", " << z << ')' << '\n';

	cout << "V1 mod = " << v1.GetMod() << '\n' << '\n';


	Vector v2(2, 3, 4);
	cout << "V2 : \t";
	v2.ShowVector();
	cout << "V2 mod = " << v2.GetMod() << '\n';

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
